# Telegram's Movie Bot by P4P1_

Movie Bot permet de rechercher les informations relatives à un film. Pour ce faire rien de plus simple, il faut juste taper la manip suivante :+1:
```bash
/movie film_a_chercher
```

## Pré-requis

Pour tester le code du bot, il faut d'abord installer sur votre ordinateur :

#### :arrow_right: Node.JS :arrow_left:

Node.JS est une plateforme logicielle libre en JavaScript orientée vers les applications réseaux. Parmi les modules natifs de Node.JS, on retrouve HTTP qui permet le développement de serveur HTTP. Il est donc possible de se passer d serveurs web tels que Nginx ou Apache lors du déploiement de sites et d'applications web développés avec Node.JS.[^1]  
Pour l'installation, vous pouvez suivre le lien du [Site offciel de Node.JS](https://nodejs.org)

#### :arrow_right: NPM :arrow_left:

[__NPM__](https://npmjs.com) est le gestionnaire de paquets officiel de Node.JS. Depuis la version 0.6.3 de Node.JS, NPM fait partie de l'environnement et est donc automatiquement installé par défaut. Il permet également d'installer des applications Node.JS disponobles sur le dépôt npm (comme par exemple `node-telegram-bot-api` qui sera utilisé dans le code de notre bot).[^2]

## Utilisation

Suivre les étapes suivantes pour créer votre Bot sur la plateforme Telegram:

1. Créer votre Bot grâce à [BotFather](https://t.me/BotFather)
2. Copier le jeton d'accès généré par BotFather
3. Coller le jeton d'accès sur le fichier index.js (à la ligne 2)
4. Exécuter la commande `node index.js`
5. C'est bon vous pouvez utiliser votre bot partout.[^3]

## Contact

Pour tout complément d'informations, vous pouvez me laisser un message sur mon mail :mailbox: <ndoyepapy@live.fr>.

## License

Ce projet est sous la licence MIT - voir le fichier [LICENSE](LICENSE) pour plus d'informations.

[^1]: Extrait de la page Wikipédia de Node.JS.

[^2]: Extrait de la page Wikipédia de NPM.

[^3]: A chaque utilisation il faudra exécuter `node index.js` car le code du bot est en local sur notre machine. Pour utiliser le bot 24h/24 (sans avoir à taper `node index.js`) il faut déployer notre bot sur un serveur de déploiement. Vous pouvez par exemple utiliser [Zeit](http://zeit.co/).
